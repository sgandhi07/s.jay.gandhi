---
title: "Coming home to roost"
description: "Our crisis and institutions"
date: 2020-10-01
tags: ["Green New Deal", "2020 Election"]
draft: false
---

The first 2020 presidential debate was a lot better than I expected. Fully because Biden didn't fall flat on his face. He managed to bring some dynamism to his usually cardboard cut-out presence; he stood his ground against an egomaniacal bully. I was apprehensive because it could've easily turned into a lackluster 2016 redux. Can you imagine if Joe rode in on a high horse? He wouldn't have been able to get a word in. We, as a nation, seem to respond solely to spectacle and that is exactly what we got.

---

With each passing day, the threat of 45 stealing the election becomes more and more real. Ralph Nader has talked about this multiple times on his radio hour/podcast:

  - {{< href "How the Right is Shredding the Vote (Sept 2020)" "https://ralphnaderradiohour.com/how-the-right-is-shredding-the-vote/" >}}
  - {{< href "Can Trump Steal 2020? (Aug 2020)" "https://ralphnaderradiohour.com/young-activists-can-trump-steal-2020/" >}}
  - {{< href "Will We Be Able To Vote? (June 2020)" "https://ralphnaderradiohour.com/will-we-be-able-to-vote" >}}

Along with Nader, Democracy Now! and The Atlantic have laid out multiple Republican strategies to delegitimize and steal this election. The fascists have already put the pedal to the metal with the {{< href "Republican legislature in swing-state Pennsylvania trying to enlarge the electoral college loophole, and undermine the will of its people" "https://www.huffpost.com/entry/donald-trump-pennsylvania-election_n_5f750df6c5b66377b27ce1b5" >}}. The end of American democracy could be approx. 5 weeks away, making way for American fascism.

---

On a personal note, I'm a Piscean and fully embody the dreaminess element of this sign. I dream that the visions of our civil rights leaders, scientists, and "leftist radicals" will become a reality. Neither Joe nor reality-denier-in-charge are going make that dream a reality; however, with organizing and mobilizing, the victory of former will give us a fighting chance, while the victory of latter ensures the death of that possibility. Linda Sarsour put it most plainly: **we are voting to choose our opponent.**

Inshallah Joe wins by a landslide, and we see him sworn in on January 20, 2021. Alongside that, I pray that Republicans/rancid right-wingers are voted out of the Senate and House, especially deposing morally-void mitch (R, KY) and wet-garbage graham (R, SC).

When that happens, the real work can start. The work of dragging and pushing the Democratic establishment to do their motherlovin' job.

We are facing two bubbling crises: climate/environmental/ecological and race/class. In this post I want to think through the climate crisis. However, there is NO way out of climate crisis without addressing and rectifying systemic racial and class-based oppression.

Christian Parenti's {{< href "'Radical Hamilton: economic lessons from a misunderstood founder'" "https://www.versobooks.com/books/3186-radical-hamilton" >}} lays the groundwork for tactful institutional-change that we NEED to face both the crises of our time.

Both, Parenti and {{< href "this webcast I just started" "https://youtu.be/wJ_jvdAP-7U?t=98" >}}, really drive across the point that **it is (almost) always in times of crisis and upheavals that certain ideas germinate into blueprints, which then turn into practices, institutions, and norms.** We are living through such times, and I pray that it's the left "radical" ideas and actions based on scientific communities' findings that prevail.

---

#### Journey through Hamilton's ideas

I thought the military-industrial complex was a 20th century phenomenon, but Parenti's *"Radical Hamilton"* explains how it is baked into the foundation of this country, courtesy of Alexander Hamilton.

As most people know, Hamilton is the architect of our banking system; he is also the founder of the AMERICAN school of economics. Fortunately Hamilton was a prodigious writer, and wrote treatises on his economic and state-building ideas. This allows Parenti to show us how Hamilton the statesman's actions were supported by Hamilton the thinker and theorist. The scaffolding that held his state-building ideas together is the **MILITARY-INDUSTIRAL-BANKING** locus.

TLDR; Hamilton consolidated a group of rag-tag states into a nation of states, called the United States of America by: forming a **national bank** and using its currency to nurture a domestic **industrial economy**. This economy was then used to further stabilize and prop the Bank while also becoming a stable source for **military** equipment/production. Wars are expensive, and a strong bank/national credit would ensure better terms for loans for future war expenses. Moreover, the military was critical to the scaffolding because the new nation needed it to stave off attacks from across the pond, other colonizers on the continent, and Indigenous populations, from whom they stole and ravaged the land. *One needs a robust, industrial economy to have a banking system capable of sustaining the military.*

**This network of banking, military, and economy(industry) was to be actively configured and maintained by the State.** Today's laisse-faire, neoliberal, free trade, free market principles are absolutely not what the AMERICAN founder of economics prescribed, *especially not in a time of crises.*

 ---

#### Hamiltonian Military-Industrial-Banking Complex Today

One of the greatest threats we now face is catastrophic climate change and wildlife destruction. We do not need the military to aim at any living being, necessarily. It needs to look out and prepare for a climate that we are completely unprepared to work with. *(Cybersecurity is another threat, but I want to look at it in another post, when I finish the book Surveillance Capitalism by Shoshana Zuboff).*

Today, we have an over-bloated, wasteful military, a fast disappearing industrial sector, and banks/financial institutions that are leeching the wealth up to the very top. (It can be argued that our economy is now composed of military and their contractors, tech behemoths, and banks/financial institutions). Re-configuring our economy by re-imagining the industrial sector is a low-hanging fruit thanks to the **GREEN NEW DEAL**.

Christian Parenti wrote the book "Radical Hamilton" to rectify the fact that Hamilton's magnum opus, *'Report on the Subject of Manufactures'* is criminally under-examined. The significance of industrial sector is central to Hamilton's thinking, and in the report, he goes into extreme detail on how the Nation **must** leverage its ability to tax, subsidize, and place tariffs to STEER the national economy. A robust manufacturing/industrial sector is essential for our survival, not only according to Hamilton, but according to a {{< href "prescient Op-Ed in Next City by Tom Outerbridge and Tinia Pina 'The Industrial Sector is at the Heart of Green New Deal.'" "https://nextcity.org/daily/entry/the-industrial-sector-is-at-the-heart-of-a-green-new-deal" >}}

{{< figure src="../next_city_article.png" link="https://nextcity.org/daily/entry/the-industrial-sector-is-at-the-heart-of-a-green-new-deal" align="center" width="50%" target="_blank" >}}

All this begs the question: why the hoop-la over industry and manufacturing? Well, the economy must be based on certain key sector(s), and the options in Hamilton's time were: industry or agrarian (Jefferson fought for an agrarian economy). As mentioned before, today's economy is based on financialization of all/most aspect of our lives, military and their contractors, and tech giants.

We need industry and manufacturing because we need to be able to build our own green technologies. We cannot rely on exports, because it leaves us susceptible to foreign vagaries, to our detriment. We need industry to implement findings from R&D (research and development), and we **NEED** R&D to start and prepare for worsening effects of climate disaster. **Above all that, it could be an immense creator of good, decent jobs.** Though definitely not a fan of his, Google's ex-leader, Eric Schmidt, recently declared that the US "dropped the ball" on innovation. He cites decrease in State funding, and he is 100% not wrong; but he does fail to recognize the link between R&D and industry. If production is offshore, it's hard to fund research which has to jump hurdles to be implemented in a foreign country. We need equipment and technology on hand that can be nimble and re-purposed. According to Outerbirdge and Pinia, it is one of the reasons that China now leads in solar energy production, even though the US developed the solar panel concept. And this continues with the lead that China is taking with AI, and other emerging, innovative technologies. We NEED industry and WE NEED R&D to strategically ready ourselves for catastrophic climate disaster and wildlife destruction, and the GREEN NEW DEAL is a giant stride in that new, hopeful direction.

---

#### What does all this mean? It means we rebuff any idiotic questions such as "who is going to pay for this?"

One: the United States was borne out of taking on debt, so it's not always a bad thing for an entity such as a nation-state to go into debt. But to put it in other words: stop giving massive tax breaks to the rich; tax the patriarch-fucking rich; tax the polluters; and STOP subsidizing corporations. Maybe re-write and simplify the tax code so that 45 is not able to pay $750 federal tax LEGALLY. The money will come flowing in. And to hammer in the point, Hamilton BEGGED the Nation to use these taxing and subsiding tools to STEER the economy.

My biggest gripe about the debate is that Joe didn't incorporate climate disaster nearly enough into his answers. This would've helped to bolster his argument for the "Biden Plan" AKA Green New Deal when the questions were about economy and manufacturing jobs. Unprecedented amount of land is being consumed by wildfires in the American west and hurricanes are bringing down more precipitation and increasing in frequency in the American southeast-- people are living through this. Just bringing up these living disasters would've been enough.

The Green New Deal offers an opportunity to re-shape and re-calibrate the military, industry, and state banking nexus, to serve current needs, using its founder's, Hamilton's, own economic logic and principles. **On a final note, re-imagining these institutions means that race and class justice is part of the same conversation.** Private prisons, militarized policing, private detention centers are part of the military, and the resources currently sucked into those oppressing endeavors need to be places towards reparations and rehabilitation, among myriad of other under-funded/un-funded issues.
